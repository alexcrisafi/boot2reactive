package com.alexcrisafi.boot2.repository;

import com.alexcrisafi.boot2.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
