package com.alexcrisafi.boot2.controller;

import com.alexcrisafi.boot2.model.Person;
import com.alexcrisafi.boot2.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonRepository repository;

    @GetMapping
    public Flux<Person> getAll() {
        return Flux.fromIterable(repository.findAll());
    }

    @GetMapping("{id}")
    public @ResponseBody
    Mono<Person> findById(@PathVariable Long id) throws InterruptedException {
        return Mono.just(repository.findById(id).orElse(Person.empty()));
    }

}
